package me.sedlar.soundloader.data;

import org.jaudiotagger.audio.exceptions.InvalidAudioFrameException;
import org.jaudiotagger.audio.exceptions.ReadOnlyFileException;
import org.jaudiotagger.audio.mp3.MP3File;
import org.jaudiotagger.tag.TagException;
import org.jaudiotagger.tag.id3.ID3v11Tag;
import org.jaudiotagger.tag.id3.ID3v1Tag;

import java.io.File;
import java.io.IOException;

/**
 * @author Tyler Sedlar
 */
public class SongFile {

    public final String dir, genre, artist, album, title, fileType;

    public SongFile(String dir, String genre, String artist, String album, String title,
                    String fileType) {
        if (!dir.endsWith(File.separator))
            dir += File.separator;
        this.dir = dir;
        this.genre = genre;
        this.artist = artist;
        this.album = album;
        this.title = title;
        this.fileType = fileType;
    }

    public String path() {
        return dir + genre + File.separator + artist + File.separator + album + File.separator + title + "." + fileType;
    }

    public boolean id3() {
        try {
            String target = path();
            MP3File mp3 = new MP3File(target);
            ID3v11Tag tag = new ID3v11Tag();
            tag.setAlbum(album);
            tag.setArtist(artist);
            tag.setTitle(title);
            tag.setGenre(genre);
            mp3.setID3v2Tag(tag);
            mp3.save();
            new File(target.replace("." + fileType, ".original." + fileType)).delete();
            return true;
        } catch (TagException | IOException | ReadOnlyFileException | InvalidAudioFrameException e) {
            e.printStackTrace();
            return false;
        }
    }
}
