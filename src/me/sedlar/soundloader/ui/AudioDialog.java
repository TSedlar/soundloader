package me.sedlar.soundloader.ui;

import me.sedlar.soundloader.SoundLoader;
import me.sedlar.soundloader.data.SongFile;
import me.sedlar.ui.DefaultTextField;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

public class AudioDialog extends JDialog {

    private final DefaultTextField artist = new DefaultTextField("Artist...");
    private final DefaultTextField title = new DefaultTextField("Title...");
    private final DefaultTextField album = new DefaultTextField("Album...");
    private final DefaultTextField genre = new DefaultTextField("Genre...");
    private final DefaultTextField target = new DefaultTextField(System.getProperty("user.home") + File.separator +
            "Music" + File.separator);

    private final DefaultTextField[] defaults = {artist, title, album, genre};

    private String url;
    private String fileType;

    public void setURL(String url) {
        this.url = url;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    private boolean isInputValid() {
        for (DefaultTextField dtf : defaults) {
            if ((dtf.getText().isEmpty()) || (dtf.getText().equals(dtf.defaultText)))
                return false;
        }
        return !this.target.getText().isEmpty();
    }

    public void reset() {
        for (DefaultTextField dtf : defaults)
            dtf.reset();
    }

    public String getArtist() {
        return artist.getText();
    }

    public String getTitle() {
        return title.getText();
    }

    public String getAlbum() {
        return album.getText();
    }

    public String getGenre() {
        return genre.getText();
    }

    public String getTarget() {
        return target.getText();
    }

    public AudioDialog() {
        setTitle("SoundLoader");
        setLayout(new FlowLayout(FlowLayout.CENTER, 0, 5));
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        setPreferredSize(new Dimension(200, 213));
        artist.setMargin(new Insets(0, 5, 0, 0));
        artist.setPreferredSize(new Dimension(185, 25));
        add(artist);
        title.setMargin(new Insets(0, 5, 0, 0));
        title.setPreferredSize(new Dimension(185, 25));
        add(title);
        album.setMargin(new Insets(0, 5, 0, 0));
        album.setPreferredSize(new Dimension(185, 25));
        add(album);
        genre.setMargin(new Insets(0, 5, 0, 0));
        genre.setPreferredSize(new Dimension(185, 25));
        add(genre);
        target.setMargin(new Insets(0, 5, 0, 0));
        target.setPreferredSize(new Dimension(185, 25));
        add(target);
        JButton download = new JButton("Download");
        download.setPreferredSize(new Dimension(185, 25));
        download.addActionListener(e -> {
            if (!isInputValid())
                return;
            setVisible(false);
            new Thread(() -> SoundLoader.download(url, new SongFile(getTarget(), getGenre(), getArtist(), getAlbum(),
                    getTitle(), fileType))).start();
        });
        add(download);
        pack();
        setResizable(false);
        setLocationRelativeTo(null);
        setModal(true);
        setAlwaysOnTop(true);
    }
}