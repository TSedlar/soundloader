package me.sedlar.soundloader;

import me.sedlar.soundloader.data.SongFile;
import me.sedlar.soundloader.listener.SoundCloudListener;
import me.sedlar.soundloader.listener.YouTubeListener;
import me.sedlar.soundloader.ui.AudioDialog;
import me.sedlar.util.concurrent.task.ClipboardTask;
import me.sedlar.util.io.DownloadManager;
import me.sedlar.util.io.Internet;
import me.sedlar.util.io.ResourceLoader;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;

/**
 * @author Tyler Sedlar
 */
public class SoundLoader {

    public static final ResourceLoader RESOURCE_LOADER = new ResourceLoader("/me/sedlar/soundloader/res/");
    public static final BufferedImage ICON;
    public static final TrayIcon TRAY;
    public static final AudioDialog DIALOG;

    public static void showMessage(String title, String message, TrayIcon.MessageType type) {
        TRAY.displayMessage(title, message, type);
    }

    public static void download(String url, final SongFile song) {
        DIALOG.reset();
        File target = Internet.download(url, song.path(), new DownloadManager() {
            public void onDownload(int percent) {
                if (percent == 100) {
                    TRAY.setImage(ICON);
                    showMessage("Download Manager", song.title + " downloaded!", TrayIcon.MessageType.INFO);
                } else {
                    BufferedImage current = new BufferedImage(16, 16, BufferedImage.TYPE_INT_ARGB);
                    Graphics g = current.createGraphics();
                    g.drawImage(ICON, 0, 0, null);
                    int width = (int) (percent * 0.16) + 1;
                    g.setColor(new Color(80, 255, 80, 100));
                    g.fillRect(0, 0, width, 16);
                    g.setFont(new Font("Arial", Font.BOLD, 9));
                    String str = Integer.toString(percent);
                    int x = 8 - (g.getFontMetrics().stringWidth(str) / 2);
                    g.setColor(Color.BLACK);
                    g.drawString(str, x - 1, 11);
                    g.drawString(str, x + 1, 11);
                    g.drawString(str, x, 10);
                    g.drawString(str, x, 12);
                    g.setColor(Color.WHITE);
                    g.drawString(str, x, 11);
                    TRAY.setImage(current);
                }
            }
        });
        if (target != null) {
            if (song.id3()) {
                showMessage("ID3 Manager", "Set ID3 tags [" + song.title + "]", TrayIcon.MessageType.INFO);
            } else {
                showMessage("ID3 Manager", "Failed to set ID3 tags [" + song.title + "]", TrayIcon.MessageType.ERROR);
            }
        }
    }

    static {
        ICON = RESOURCE_LOADER.getImage("images/icon_16x16.png");
        TRAY = new TrayIcon(ICON);
        PopupMenu menu = new PopupMenu();
        MenuItem exit = new MenuItem("Exit");
        exit.addActionListener(e -> {
            System.gc();
            System.runFinalization();
            System.exit(0);
        });
        menu.add(exit);
        TRAY.setPopupMenu(menu);
        try {
            SystemTray.getSystemTray().add(TRAY);
        } catch (AWTException e) {
            e.printStackTrace();
            System.exit(0);
        }
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException | UnsupportedLookAndFeelException | IllegalAccessException |
                InstantiationException e) {
            e.printStackTrace();
        }
        DIALOG = new AudioDialog();
    }

    public static void main(String[] args) {
        ClipboardTask clipboard = new ClipboardTask();
        clipboard.push(new YouTubeListener());
        clipboard.push(new SoundCloudListener());
        clipboard.start();
    }
}
