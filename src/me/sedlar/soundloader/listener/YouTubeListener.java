package me.sedlar.soundloader.listener;

import me.sedlar.soundloader.SoundLoader;
import me.sedlar.soundloader.data.SongFile;
import me.sedlar.util.io.DownloadManager;
import me.sedlar.util.io.Internet;
import org.jaudiotagger.audio.AudioFile;
import org.jaudiotagger.audio.AudioFileIO;
import org.jaudiotagger.tag.Tag;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.*;
import java.util.List;

/**
 * @author Tyler Sedlar
 */
public class YouTubeListener implements SoundListener {

    public static final String CONVERT_2_MP3 = "http://www.convert2mp3.cc/check.php?v=";
    public static final String DOWNLOADER_SPACE = "http://dl%s.downloader.space/dl.php?id=%s";

    @Override
    public BufferedImage getDefaultTrayIcon() {
        return SoundLoader.ICON;
    }

    @Override
    public void onContentSet(String content) {
        if (content != null) {
            if ((content.contains("youtube.com")) && (content.contains("v="))) {
                String vid = content.split("v=")[1];
                if (vid.contains("&"))
                    vid = vid.substring(0, vid.indexOf("&"));
                String directLink = findFile(vid);
                if (directLink != null) {
                    SoundLoader.DIALOG.setFileType("mp3");
                    SoundLoader.DIALOG.setURL(directLink);
                    SoundLoader.DIALOG.setVisible(true);
                }
            }
        }
    }

    private static String findFile(String vid) {
        String data = Internet.read(CONVERT_2_MP3 + vid).get(0);
        String[] splits = data.split("\\|");
        String download = splits[1];
        String id = splits[2];
        return String.format(DOWNLOADER_SPACE, download, id);
    }
}