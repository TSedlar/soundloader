package me.sedlar.soundloader.listener;

import me.sedlar.util.concurrent.task.ClipboardTask.ClipboardListener;

import java.awt.image.BufferedImage;

/**
 * @author Tyler Sedlar
 */
public interface SoundListener extends ClipboardListener{

    public BufferedImage getDefaultTrayIcon();
}
