package me.sedlar.soundloader.listener;

import me.sedlar.soundloader.SoundLoader;
import me.sedlar.util.io.Internet;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.List;

/**
 * @author Tyler Sedlar
 */
public class SoundCloudListener implements SoundListener {

    public static final String DEVELOPER_CLIENT_ID = "609ae0b573913db156968e0ec38c1e26";

    public static final String API = "https://api.sndcdn.com/";
    public static final String RESOLVE = "resolve?";
    public static final String URL_QUERY = "url=";
    public static final String STATUS_CODE_MAP_QUERY = "_status_code_map%5B302%5D=200";
    public static final String JSON_FORMAT_QUERY = "_status_format=json";
    public static final String CLIENT_ID_QUERY = "client_id=";

    public static final String TRACKS = API + "i1/tracks/";
    public static final String STREAMS = "streams?";

    @Override
    public BufferedImage getDefaultTrayIcon() {
        return SoundLoader.ICON;
    }

    @Override
    public void onContentSet(String content) {
        if (content != null && content.contains("soundcloud.com")) {
            String url = API + RESOLVE + URL_QUERY + content + "&" + STATUS_CODE_MAP_QUERY + "&" + JSON_FORMAT_QUERY +
                    "&" + CLIENT_ID_QUERY + DEVELOPER_CLIENT_ID;
            try {
                List<String> page = Internet.read(url);
                if (page != null) {
                    String source = page.get(0);
                    if (source != null) {
                        String track = source.split("/tracks/")[1].split("\\?")[0];
                        url = TRACKS + track + "/" + STREAMS + CLIENT_ID_QUERY + DEVELOPER_CLIENT_ID;
                        page = Internet.read(url);
                        if (page != null) {
                            source = page.get(0);
                            if (source != null) {
                                url = source.split("\"http_mp3_128_url\":\"")[1].split("\"")[0].replaceAll("\\\\u0026", "&");
                                SoundLoader.DIALOG.setFileType("mp3");
                                SoundLoader.DIALOG.setURL(url);
                                SoundLoader.DIALOG.setVisible(true);
                            }
                        }
                    }
                }
            } catch (Exception e) {
                SoundLoader.showMessage("Download Manager", "Failed to download", TrayIcon.MessageType.ERROR);
            }
        }
    }
}
