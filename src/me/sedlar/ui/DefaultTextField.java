package me.sedlar.ui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class DefaultTextField extends JTextField {

    public final String defaultText;

    public void reset() {
        setText(defaultText);
        setCaretPosition(0);
        setFont(new Font(getFont().getName(), Font.ITALIC, getFont().getSize()));
    }

    public DefaultTextField(final String defaultText) {
        this.defaultText = defaultText;
        reset();
        addMouseListener(new MouseAdapter() {
            public void mousePressed(MouseEvent e) {
                if (getText().equals(defaultText)) {
                    setCaretPosition(0);
                    setFont(new Font(getFont().getName(), Font.ITALIC, getFont().getSize()));
                } else {
                    setFont(new Font(getFont().getName(), Font.PLAIN, getFont().getSize()));
                }
            }
        });
        addKeyListener(new KeyAdapter() {
            public void keyPressed(KeyEvent e) {
                if (getText().equals(defaultText)) {
                    if (e.getKeyCode() == KeyEvent.VK_BACK_SPACE) {
                        e.consume();
                    } else {
                        setText("");
                        setFont(new Font(getFont().getName(), Font.PLAIN, getFont().getSize()));
                    }
                }
            }

            public void keyReleased(KeyEvent e) {
                if (getText().isEmpty())
                    reset();
            }
        });
    }
}