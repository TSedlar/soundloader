package me.sedlar.util;

/**
 * @author Tyler Sedlar
 */
public class Time {

    public static void sleep(int millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException ignored) {}
    }
}
