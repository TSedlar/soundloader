package me.sedlar.util.io;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;

/**
 * @author Tyler Sedlar
 */
public class ResourceLoader {

    private final String dir;

    public ResourceLoader(String dir) {
        if (!dir.endsWith("/"))
            dir += "/";
        this.dir = dir;
    }

    public BufferedImage getImage(String img) {
        try (InputStream in = ResourceLoader.class.getResourceAsStream(dir + img)) {
            return ImageIO.read(in);
        } catch (IOException e) {
            return null;
        }
    }
}
