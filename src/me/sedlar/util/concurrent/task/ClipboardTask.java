package me.sedlar.util.concurrent.task;

import me.sedlar.util.Clipboard;
import me.sedlar.util.concurrent.LoopTask;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Tyler Sedlar
 */
public class ClipboardTask extends LoopTask {

    public static interface ClipboardListener {

        public void onContentSet(String content);
    }

    private final List<ClipboardListener> listeners = new ArrayList<>();

    public void push(ClipboardListener listener) {
        listeners.add(listener);
    }

    public void pull(ClipboardListener listener) {
        listeners.remove(listener);
    }

    private String content = null;

    @Override
    public int loop() {
        if (content == null)
            content = Clipboard.getContent();
        if (content != null) {
            String current = Clipboard.getContent();
            if (current != null && !current.equals(content)) {
                content = current;
                for (ClipboardListener listener : listeners)
                    listener.onContentSet(content);
            }
        }
        return 1000;
    }
}
