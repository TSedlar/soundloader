package me.sedlar.util.concurrent;

import me.sedlar.util.Time;

/**
 * @author Tyler Sedlar
 */
public abstract class LoopTask extends Thread {

    private boolean alive = false;

    public abstract int loop();

    public void interrupt() {
        super.interrupt();
        alive = false;
    }

    @Override
    public final void run() {
        alive = true;
        while (alive) {
            int sleep;
            try {
                sleep = loop();
            } catch (Exception e) {
                sleep = -1;
            }
            if (sleep < 0) {
                interrupt();
                return;
            }
            Time.sleep(sleep);
        }
    }
}